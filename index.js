var TwitterPackage = require('twitter');
var request = require('request');

console.log('Hello World! I am a twitter bot!');

if(process.env.NODE_ENV!='production'){
    require('dotenv').load();
}

var secret = {
    consumer_key: `${process.env.TWITTER_CONSUMER_KEY}`,
    consumer_secret: `${process.env.TWITTER_SECRET_KEY}`,
    access_token_key: `${process.env.TWITTER_ACCESS_TOKEN_KEY}`,
    access_token_secret: `${process.env.TWITTER_ACCESS_TOKEN_SECRET}`
}

var Twitter = new TwitterPackage(secret);
getAllSourcesAndTweet();

function topNewsTweeter(newsSource,screen_name,status_id){
    request({
        url: 'https://newsapi.org/v1/articles?source=' + newsSource + '&apiKey=' + process.env.NEWS_API_KEY,
        method: 'GET'
    }, (error,response,body)=>{
        if(!error && response.statusCode == 200){
            var botResponse = JSON.parse(body);
            console.log(botResponse);
            tweetTopArticle(botResponse.articles,screen_name)
        }else{
            console.log('Sorry, no news');
        }
    });
}

function tweetTopArticle(articles,screen_name,status_id){
    var article = articles[0];
    tweet(article.title + " " + article.url, screen_name)
}

function tweet(statusMsg, screen_name, status_id){
    var msg = statusMsg;
    if(screen_name!=null){
        msg = '@' + screen_name + ' ' + statusMsg
    }
    console.log('Tweet:' + msg);
    Twitter.post('statuses/update', {status: msg}, (err,response)=>{
        if(err){
            console.log('Something went wrong whil tweeting');
            console.log(err);
        }else if(response){
            console.log('Tweeted');
            console.log(response);
        }
    });
}
function getAllSourcesAndTweet(){
    var sources = [];
    console.log('getting sources...')
    request({
            url: 'https://newsapi.org/v1/sources?apiKey='+process.env.NEWS_API_KEY,
            method: 'GET'
        },
        function (error, response, body) {
            //response is from the bot
            if (!error && response.statusCode == 200) {
                // Print out the response body
                var botResponse = JSON.parse(body);
                for (var i = 0; i < botResponse.sources.length; i++){
                    //console.log('adding.. ' + botResponse.sources[i].id)
                    sources.push(botResponse.sources[i].id)
                }
                
                tweetFromRandomSource(sources, null, null);  
                
            } else {
                console.log('Sorry. No news sources!');
            }
        });
}


function tweetFromRandomSource(sources, screen_name, status_id){
    var max = sources.length;
    var randomSource = sources[Math.floor(Math.random() * (max + 1))];
    topNewsTweeter(randomSource, screen_name, status_id);
}
